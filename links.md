## core contracts

- [gate-state](https://better-call.dev/babylon/KT1GZ2186Kuarw43NDyJ8baoDZU9gkyL8tWU/state)
- [gate-script](https://better-call.dev/babylon/KT1GZ2186Kuarw43NDyJ8baoDZU9gkyL8tWU/script)
- [root-state](https://better-call.dev/babylon/KT19M2wqUztaXnwzBWDxuyWTbuKJDTj52DcZ/state)
- [root-script](https://better-call.dev/babylon/KT19M2wqUztaXnwzBWDxuyWTbuKJDTj52DcZ/script)
- [type-state](https://better-call.dev/babylon/KT1VEshzR54T2FzrZvE7QKG8yZWvHLj5A7iw/state)
- [type-script](https://better-call.dev/babylon/KT1VEshzR54T2FzrZvE7QKG8yZWvHLj5A7iw/script)

## level-1 teznames

- [tezossoutheastasia](https://better-call.dev/babylon/KT1BxNeUiQKrdmYjFBedJRrdeph3fsTfp52P/state)
- [tsa](https://better-call.dev/babylon/KT1HPYvqztdtycBAQRCSChTyfJUktjbPmkaZ/state)
- [9chs](https://better-call.dev/babylon/KT1Nda7WHjAcU5mf8rURZoNbPMYJXSHaEp1w/state)
- [caleb](https://better-call.dev/babylon/KT1SqJEFMcUmSqoMUgM5FvUj9ExH45y3Hm8M/state)
- [yunyan](https://better-call.dev/babylon/KT19MdkAaH7JdmahF3wRMWnLazz3Sbx8zdcB/state)
