# What is tezNames?

<img src="img/teznames_logo.jpg" alt="drawing" width="250"/>

**tezNames** is a next-generation name service on the [tezos](https://tezos.foundation/) blockchain that incorporates enhanced functionality to tackle the practical needs of a
constantly evolving set of challenges in the Internet namespace. We hope to build a stable name registration platform which allows innovation to unlock new possibilities
in name management, valuation, interoperability and not to mention security.

## User Guide

We maintain a [User guide](https://teznames.readthedocs.io/) that explains more about each individual component of the service.

## Contributions

We are always looking out for contributors who can suggest enhancements to **teznames**. Here are some ways you can get involved and help the community by improving tezNames!

### contribute towards documentation

The [User guide](https://teznames.readthedocs.io/) is located in the `docs` branch. If you're willing to help us to improve our document and users guide, please fork and create a branch for editing. Once you finish your work, please issue an _merge request_ to the `docs` branch so that we can review.

### contribute code

So far the main development branch used is `master`. If you would like to contribute or suggest fixes to code, please fork and create a branch for editing. Once the changes have been made, please issue an _merge request_ to our `master` branch.

### report bugs

If you found any errors or bugs, please kindly inform us at [teznames/issues](https://gitlab.com/tezos-southeast-asia/teznames/issues) by opening a new issue and tag it with the preset Label: `bug`! In any bug or error report, please try to provide as many details as possible,
including but not limited to detailed steps on how to replicate the issue, a rough description of the working environment you are using, any anomalies or persisting patterns you notice.

### general feedback

If you would like to provide feedback or suggest features for future enhancements, please go to [teznames/issues](https://gitlab.com/tezos-southeast-asia/teznames/issues) page to open a new issue and tag it with the preset Label: `discussion` (for feedback) or `suggestion` (for features)!
