#!/bin/bash
#
if [ "$1" == "" ]; then
  basedir=$(git rev-parse --show-toplevel) || exit 1
else
  basedir="$1"
fi
echo -e "\033[36m[info]\033[39m set basedir as ${basedir}"
#
echo -e "\033[36m[info]\033[39m loading ${basedir}/ENV.."
source "${basedir}/ENV" || exit 1
echo -e "\033[36m[info]\033[39m loading ${basedir}/ADDR.."
source "${basedir}/ADDR" || exit 1
echo -e "\033[36m[info]\033[39m loading ${basedir}/L1TEZNAMES.."
source "${basedir}/L1TEZNAMES" || exit 1
#
echo -e "\033[36m[info]\033[39m reseting ${basedir}/links.md.."
if [ -f "${basedir}/links.md" ]; then
  rm "${basedir}/links.md"
  touch "${basedir}/links.md"
else
  touch "${basedir}/links.md"
fi
#
echo -e "## core contracts" >> "${basedir}/links.md"
echo -e " " >> "${basedir}/links.md"

echo -e "- [gate-state](https://better-call.dev/babylon/${GATEADDR}/state)"  >> "${basedir}/links.md"
echo -e "- [gate-script](https://better-call.dev/babylon/${GATEADDR}/script)" >> "${basedir}/links.md"

echo -e "- [root-state](https://better-call.dev/babylon/${ROOTNAMEADDR}/state)"  >> "${basedir}/links.md"
echo -e "- [root-script](https://better-call.dev/babylon/${ROOTNAMEADDR}/script)" >> "${basedir}/links.md"

echo -e "- [type-state](https://better-call.dev/babylon/${NAMETYPESADDR}/state)"  >> "${basedir}/links.md"
echo -e "- [type-script](https://better-call.dev/babylon/${NAMETYPESADDR}/script)" >> "${basedir}/links.md"

echo -e " " >> "${basedir}/links.md"
echo -e "## level-1 teznames" >> "${basedir}/links.md"
echo -e " " >> "${basedir}/links.md"

echo -e "- [tezossoutheastasia](https://better-call.dev/babylon/${tezname_04_tezossoutheastasia}/state)"  >> "${basedir}/links.md"
echo -e "- [tsa](https://better-call.dev/babylon/${tezname_04_tsa}/state)"  >> "${basedir}/links.md"
echo -e "- [9chs](https://better-call.dev/babylon/${tezname_04_9chs}/state)"  >> "${basedir}/links.md"
echo -e "- [caleb](https://better-call.dev/babylon/${tezname_04_caleb}/state)"  >> "${basedir}/links.md"
echo -e "- [yunyan](https://better-call.dev/babylon/${tezname_04_yunyan}/state)"  >> "${basedir}/links.md"
#
echo -e "\033[36m[info]\033[39m link file have been generated successfully"
#
