import Data.String
import Data.List
import System.IO
import System.Environment
import Text.Parsec

--
main :: IO ()
main = do
  inData <- readFile "tt"
  let x = map goo $ map foo $ map words $ lines inData
  -- pprint  x
  --
  putStrLn "----- Hashed Name"
  let hname = head $ (head . drop 1) x
  putStrLn $ show hname
  --
  putStrLn "----- Destination"
  let dest = parse parseYY "" $ head $ (head . drop 4) x
  putStrLn $ show dest
  --
  putStrLn "----- Type of Destination"
  let destT = hoo $ (head . drop 5) x
  putStrLn $ show destT
  --
  putStrLn "----- Owner"
  let owner = (init . tail) $ head $ (head . drop 6) x
  putStrLn $ show owner
  --
  putStrLn "----- Type of Name"
  let nameT = hoo2 $ (head . drop 7) x
  putStrLn $ show nameT
  --
  putStrLn "----- Fees"
  let fees = parse parseFees "" $ init $ init $ unwords $ head $ drop 8 x
  putStrLn $ show fees
  --
  putStrLn "----- Subnames"
  let z = map (\x -> if x == '\n' then ' ' else x) $ unlines $ map unwords $ init $ init $ drop 9 x
  let subs = parse parseRecords "" z
  putStrLn $ show subs
  --
  putStrLn "========================="
  putStrLn "----- WHOIS"
  let aa = WHOIS hname (fromRight dest) destT owner nameT (fromRight fees) (fromRight subs)
  print aa
  --

fromRight :: Either a b -> b
fromRight (Right x) = x

pprint :: (Show a) => [a] -> IO ()
pprint [] = return ()
pprint (x:xs) = do
  print x
  pprint xs

parseFees :: Parsec String st (Int, Int, Int)
parseFees = do
  spaces
  f1 <- many1 digit
  spaces
  f2 <- many1 digit
  spaces
  f3 <- many1 digit
  spaces
  return (read f1, read f2, read f3)

foo xs = filter (\ x -> ((x /= "Pair") && (x /= "(Pair"))) xs

goo ("(Left" : "Unit))" : xs) = "Wallet" : (goo xs)
goo ("(Right" : "(Left" : "Unit)))" : xs) = "IP" : (goo xs)
goo ("(Right" : "(Right" : "Unit)))" : xs) = "URL" : (goo xs)
goo (x:xs) = x : (goo xs)
goo [] = []


hoo :: [String] -> String
hoo ["(Left","Unit)"]      = "Wallet"
hoo ["(Right", "(Left",x]  = "IP"
hoo ["(Right", "(Right",x] = "URL"
hoo x = unwords x

hoo2 :: [String] -> String
hoo2 ["(Left","Unit)"]      = "Regular"
hoo2 ["(Right", "(Left",x]  = "Premium"
hoo2 ["(Right", "(Right",x] = "Restricted"
hoo2 x = unwords x

-- -- -- -- --

data REntity = REntity String String String deriving (Show, Eq)

parseRecords :: Parsec String st [REntity]
parseRecords = do
    char '{'
    spaces
    argts <- sepBy parseRecord (spaces >> char ';' >> spaces)
    spaces
    char '}'
    return argts

parseRecord :: Parsec String st REntity
parseRecord = do
    spaces
    string "Elt"
    spaces
    a <- many1 alphaNum
    spaces
    char '"'
    b <- many1 alphaNum
    char '"'
    spaces
    c <- many1 alphaNum
    spaces
    return (REntity a b c)

-- -- -- -- --

data WHOIS = WHOIS
  { hname :: String
  , dest  :: String
  , destT :: String
  , owner :: String
  , nameT :: String
  , fees  :: (Int, Int, Int)
  , subs  :: [REntity]
  } deriving (Show, Eq)

-- -- -- -- --
parseYY :: Parsec String st String
parseYY = do
    many $ char '"'
    x <- many1 $ choice [alphaNum, char '.', char '_' , char '-', char '/', char '?']
    many $ char '"'
    return x
