<img src="img/teznames_logo.jpg" alt="logo" width="250"/>

[**tezNames**](https://teznames.readthedocs.io/en/latest/system/overview/) is a name registration service for [Tezos](https://tezos.com/) blockchain. _tezName_ is powered by [Tezos contracts](https://teznames.readthedocs.io/en/latest/contracts/)

_tezNames_ does not have any client-side behavior. You can think of _tezNames_ as a normal Tezos contract which you can interact with using _tezos-client_. However, we highly recommand you to use [**dough**](https://gitlab.com/9chapters/dough), a command line tool developed by [**Nine Chapters**](https://www.9chapters.io/) to interact with _tezNames_. You can find out more from the [source code](https://gitlab.com/9chapters/dough) and the [users guide](https://dough.readthedocs.io/en/latest/).
