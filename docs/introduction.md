The **tezNames** is a name registration system for the Tezos network. With tezNames, we can replace a a long hash wallet addressw with a human-readable name(tezName) instead.


On tezNames, you can **query** any tezName to obtain its _destination_, _ownership_ or other _information_. If you want to own that tezName, the site provides instruction on how you can **register** your own tezNames on tezNames. The destination of your tezName will be your wallet by default. Once you have successfully regiestered your tezName, you can **update** its _destination_ at any time. Additionally you can transfer your tezName by updating its _owner address_. Your tezName ownership will last for a year and if that is too short? Don't worry! You can **renew** it during the last month of your ownership. This will extend your ownership for another year.

## tezNames in Detail

A tezNames is a contract instance on Tezos blockchain network. Every tezNames instance contains information in its storage and provides services for different user roles via contract entries. Two of the most important information are: its _owner_ and _destination_. Both of them will point into a tz1 address.

!!! info "about destination"

    The destination of a tezNames is a tezos wallet address so far. But it can be extended to support different types of destination.

All other important features will be introduced as follows.

### Subnames

A tezName may have multiple _subnames_. For exmaple, the tezName `smile.happy.tez` is a subname of `happy.tez`. Any tezName is can be a subname of another tezName.

### Types of tezName

A tezName can be classified into 3 different catergories and these catergories are as follows:

1. _Restricted_ - any trademark or legal term:  google, japan,
2. _Premium_- any popular or interesting term: blog, news, university, store
3. _Regular_ - not any one of above two: today, happy, john

A tezName is catorgorized according to the words it contain. The tezNames system will maintain two public lists with all the defined Premium and Restricted words. Users can query this information at any time.

!!! info

    There will be a fourth catergory: _forbidden_ in the future. Examples of forbidden word are any vulgar or inappropriate word.

To register a restricted tezName, the registrant will need to provide a digital legal document to prove that registrant has the proper rights to legally own this name.

### Dates

There is some information on every tezName. Including

- The start date of the current ownership
- The end date of the current ownership
- Last date of modification of the tezNaME

Those information are public so that anyone can inquire as well.

## Registering a tezName

The tezName registrant will need to pay a _commission_ to tezNames system and that commission is based on the tezName category. For exmaple, to register or renew a _regular_ name, you will need to pay **10tz** while if you want to own a premium or restricted name the commission will be higher.

## Update a tezName

A tezName owner can "modify" two information at any time - the _owner_ and the _destination_. Please note that, once you change the owner address for your tezName, you will lose the ownership immediately but the expiration time will remain the same.

## Renew a ownership

For now, the tezNames system supports a simplest renewing mechanism - a tezName owner can renew an ownership simply by paying the _commission_ again. An owner can only renew a tezName during the last month of its ownership.
