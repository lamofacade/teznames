!!! info

    The current protocol are **9chsTNS-0.0.1** and **9chsTNRS-0.0.1**. The versioning policy follows [PVP](https://pvp.haskell.org/).

??? tldr

    + **teznames** - a system for name registration on and for tezos network
    + **tezname** - a name entity on _teznames_ system
    + **tezname records** - a list of records of _registered tezname_
    + **cTezName** - contract defines one _tezname_. A `.tz` file
    + **cTNRecords** - contract defines one _tezname records_. A `.tz` file.

The **teznames** is a system for name registration on and for tezos network. Conceptually the _teznames_ system consists of two parts: **tezname** and **tezname records**.

## tezname

A **tezname** is represented by an instance of contract on tezos network. Every user can _inquire_ information for one specific name. He/she can also _register a name_ by _deploying_ (aka _originating_ in tezos world) if he/she want to own a name. A _tezname_ is defined by contract **cTNRecords** which can be inferred to file `cTNRecords.tz`.

The name on _tezname_ is stored as plain text. And there are three roles will get involved for interacting with a _tezname_.

+ user - a regular user
+ owner - a actual registrant of this tezname
+ admin - a centeral manager of this tezname (will be removed soon)

For a **user**, he/she can look up public information on ant tezname as long as he/she has its address. This is actually require no operation at all. It just a simple storage reading.

For an **owner**, he/she can _renew_ the tezname which belongs to him/her. This renewing will extend this ownership by one year. Only the owner can do the renew operation.

For an **admin**, he/she can (1) _change (register) the ownership_ of an avaliable tezname; (2) forcely expire (free) a contract; (3) change (update) identity of _root_ and _admin_. (The _root_ and _admin_ are actually the same role here. We just writes in this way for proceeding some testing.)

!!! info

    If you read the source code of _cTezName_, you can find out there is another role named **root**. A root is a super user existing only for testing and demoing and it will be removed in the next update.

!!! info

    One key feature here is how we present an identity on this contract. The basic idea is to use a public key as an identity and use its signature as the proof of ownership. This concept of this signature-based identity includes two parts. The first part, _how to present identity as data_ has been written in contract **cTezName** indeed. But, the other part, _how to interact with this identity without causing too much trobles for user_, is written in [dough](https://gitlab.com/9chapters/dough/blob/master/src/Dough/Handler/InvokeHandler.hs).

## tezname records

A **tezname records** is _an instance of tezname records contract_ that maintains records of all registerred tezname and its corresponding contract address. The _tezname records_ is defined by contract **cTNRecords** which can be inferred to file `cTNRecords.tz`.

For user who just want to _resolve_ a tezname, he/she just need to look up the public information (aka _storage_ in tezos world) on this contract.

Currently, _teznames_ has a working tezname records contract at this address [KT1P4fir3MtexzztWbybNxonCv28Fu6hwmGA](https://baking-bad.github.io/better-call-dev/#alpha:KT1P4fir3MtexzztWbybNxonCv28Fu6hwmGA) on alphanet. (Please open the storage tab to see the storage. All the tezname records will be listed in `sRecord`.)
