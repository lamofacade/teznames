


## how to testing tezname (inhouse)

Please notice that: `node2.sg.tezos.org.sg` is just a sinple node running babylonnet-node. It's ok for internal testing, but we can't use it publicly.

**step 1** : get free tez from Tezos Faucet

go to [https://faucet.tzalpha.net/](https://faucet.tzalpha.net/) to apply and download a json file.

**step 2** : active account and tez (on babylon)

```bash
tezos-client activate account <alias of this wallet> with <tz1__xxxxxxxxx__.json from faucet>
```

**step 3** : create second account (on babylon)

```bash
tezos-client005 -A node2.sg.tezos.org.sg gen keys <alias of this wallet>
```

This account will contain ZERO tez.

**step 4** : using one of your account to apply a registration onto teznames-cate

```bash
tezos-client -A node2.sg.tezos.org.sg \
  transfer 10 \
  from <your wallet account> \
  to KT1AVZKhHkaf1h8ewT2JbpXC49u8xP1K6erz \
  --entrypoint '_Liq_entry_register' \
  --arg "(Pair <full name> (Pair <hash of the first word of full name> (Left Unit)))"
```

PS. if `<full name>` is "**blog.tez**", then `<hash of the first word of full name>` is "**0xce5af1d55d3e50e31d4bdd27546beb65bcb5e5fd29d21d39c536db7badb53dc1**" which can be computed from `https://api.tezos.id/mooncake/babylonnet/teznames/v0.4/babylon/hashWord?word=blog`.

**step 5** : infom system admin and wait for registration resolving

**step 6** : change the destination of teznames to your second account

```bash
tezos-client -A node2.sg.tezos.org.sg \
    transfer 0 \
    from <owner wallet address> \
    to <teznames' address> \
    --entrypoint '_Liq_entry_update' \
    --arg 'Left "<new owner wallet address>"'
```

**step 7** : change the ownership of teznames to your second account

```bash
$ tezos-client -A node2.sg.tezos.org.sg \
    transfer 0 \
    from <owner wallet address> \
    to <teznames' address> \
    --entrypoint '_Liq_entry_update' \
    --arg 'Right "<new destination>"'
```

**step 8** : renew a tezname

For testing renew, please register another tezname on gate and inform system admin that which registerring tezname you want to setup for renew. After that, you will get an almost expired tezname so that you can renew it.

```bash
tezos-client -A node2.sg.tezos.org.sg \
    transfer 10 \
    from <owner wallet address> \
    to <teznames' address> \
    --entrypoint '_Liq_entry_renew' \
    --arg 'Unit'
```
