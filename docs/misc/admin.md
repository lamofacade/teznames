!!! info "9chsTNS-0.0.1"

!!! danger "To be deprecated"

    To fully decentralize **teznames**, the _admin_ role will _be removed_ in the near future and all the functions in this page will be removed or re-implemented.

The current design of teznames includes a special role - admin. The admin is a super user like a manage of a tezname contract. Admin is the one who can manually expire a tezname or even change the registrant (i.e. owner) of a tezname.

## expire a tezname

From the admin's perspective, to expire (free) a tezname is very simple. The admin should provide three information,

+ a `#key`
+ a `#signature`
+ a `#bytes`.

to invoke the _expire_ (_free_) operation. Those information is for verifying if the invoker is turly the admin.

!!! tip "Using dough"

    To ask admin (which is also a user) to give those information for interacting with contract is not a good way to do thing. Luckly, the [dough](https://gitlab.com/9chapters/dough/blob/master/src/Dough/Handler/InvokeHandler.hs) can do this for us, so the only thing this teznames' admin need to do is to provide the following data:

    ```haskell
    InvokeArgs
       { ivkTransfer = 0
       , ivkInfo     = TNS_Free
       }
    ```

    and use `dough invoke` to trigger a tezname. For instance:

    ```bash
    > dough invoke -c dough.config -n mico.tez -i ./test/admin/free.data
    ```

## change the ownership

Changing the ownership of a tezname is just another entry invoking.

!!! tip "Using dough"

    The best part of using _dough_ is, it save your time! Although it seems we have to prepare several different data for invoking `register`. In fact, with the help of `dough`, the only information we need to provide is the alias of new owner! So the data required for `register` will look like this:

    ```haskell
    InvokeArgs
       { ivkTransfer = 0
       , ivkInfo     =
          TNS_Register
             { newOwner = "abpS" }
       }
    ```

    And with that, we can use the following instruction to register a tezname.

    ```bash
    dough invoke -c dough.config -n nicetry.tez -i ./test/admin/register.data
    ```
